package main

import (
  "MikeAustin71/DecimalNumStr/common"
	"fmt"
)

/*
import (
	"fmt"
  "MikeAustin71/DecimalNumStr/common"
)

 */

func main()  {

	TestMultiply()

}

func TestMultiply() {
	nStr1 := "35.123456"
	nStr2 := "47.9876514"
	nStr3 := "1685.4921624912384"
	nDto := common.NumStrDto{}.New()

	n1, _ := nDto.ParseNumStr(nStr1)
	n2, _ := nDto.ParseNumStr(nStr2)


	nResult, err := nDto.MultiplyNumStrs(n1, n2)

	if err != nil {
		fmt.Printf("Received error from nDto.MultiplyNumStrs(n1, n2). Error= %v \n", err)
		return
	}

	fmt.Println("Expected Result = ", nStr3)
	common.PrintNumStrDtoContents("Multiplication Result", nResult)

	fmt.Println()
	fmt.Println("=========================================")
	fmt.Println("       Successful Completion")
	fmt.Println("=========================================")



}

func TestAdd() {
	nStr1 := "-6"
	nStr2 := "67.521"
	nStr3 := "61.521"


	nDto := common.NumStrDto{}.New()

	n1, _ := nDto.ParseNumStr(nStr1)
	n2, _ := nDto.ParseNumStr(nStr2)
	nResult, _ := nDto.ParseNumStr(nStr3)
	n1OutDto, err := nDto.AddNumStrs(n1, n2)


	if err != nil {
		fmt.Printf("Received error from nDto.AddNumStrs(n1, n2). Error= %v \n",  err)
		return
	}

	fmt.Println("Addition Result")
	fmt.Println("nStr1 + nStr2")
	fmt.Println("          nStr1: ", nStr1)
	fmt.Println("          nStr2: ", nStr2)
	fmt.Println("Expected Result: ", nResult.NumStrOut)
	fmt.Println("=========================")


	common.PrintNumStrDtoContents("Addition Result", n1OutDto)

	fmt.Println()
	fmt.Println("Expected Addition Result")
	fmt.Println("=========================")
	common.PrintNumStrDtoContents("Expected Addition Result", nResult)

	fmt.Println()
	fmt.Println("================================")
	fmt.Println("Successful Completion")

}

func TestFormat() {
	nStr1 := "-12567.218956"
	nStr2 := "-9211.40"


	nDto := common.NumStrDto{}.New()

	n1, _ := nDto.ParseNumStr(nStr1)
	n2, _ := nDto.ParseNumStr(nStr2)

	n1OutDto, n2OutDto, compare, _, err := nDto.FormatForMathOps(n1, n2)


	if err != nil {
		fmt.Printf("Received error from nDto.FormatForMathOps(n1, n2). Error= %v \n",  err)
		return
	}

	fmt.Println("nStr1 Format Result")
	fmt.Println("nStr1: ", nStr1)
	fmt.Println("compare: ", compare)
	fmt.Println("=========================")


	common.PrintNumStrDtoContents(nStr1, n1OutDto)


	fmt.Println("nStr2 Format Result")
	fmt.Println("nStr2: ", nStr2)
	fmt.Println("=========================")
	common.PrintNumStrDtoContents(nStr2, n2OutDto)


	fmt.Println()
	fmt.Println("================================")
	fmt.Println("Successful Completion")

}

func TestSubtract() {
	nStr1 := "-9589.21"
	nStr2 := "-9211.40"


	nDto := common.NumStrDto{}.New()

	n1, _ := nDto.ParseNumStr(nStr1)
	n2, _ := nDto.ParseNumStr(nStr2)

	n1OutDto, err := nDto.SubtractNumStrs(n1, n2)


	if err != nil {
		fmt.Printf("Received error from nDto.SubtractNumStrs(n1, n2). Error= %v \n",  err)
		return
	}

	fmt.Println("nStr1 Subtraction Result")
	fmt.Println("nStr1 Minus nStr2")
	fmt.Println("nStr1: ", nStr1)
	fmt.Println("nStr2: ", nStr2)
	fmt.Println("=========================")


	common.PrintNumStrDtoContents("Subtraction", n1OutDto)

	fmt.Println()
	fmt.Println("================================")
	fmt.Println("Successful Completion")

}

func TestCompareSignedVals() {
	nStr1 := "-12567.218956"
	nStr2 := "-9211.40"
	expectedCompare := -1
	nDto := common.NumStrDto{}.New()
	n1, _ := nDto.ParseNumStr(nStr1)
	n2, _ := nDto.ParseNumStr(nStr2)

	cmpSgn := nDto.CompareSignedVals(&n1, &n2)

	fmt.Println("nStr1: ", nStr1)
	fmt.Println("nStr2: ", nStr2)
	fmt.Println("Comparison Result: ", cmpSgn)
	if cmpSgn != expectedCompare {
		fmt.Printf("Compared Signed Values n1= '%v' and n2='%v'. Expected Comparison= '%v'. Instead got '%v'\n",nStr1, nStr2, expectedCompare, cmpSgn)
		return
	}

	fmt.Println("================================================")
	fmt.Println("          Successful Completion")
	fmt.Println("================================================")

}
